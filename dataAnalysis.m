%% RSSI
% indices of nodes
node1Index = find(Node == 1, 1);
node2Index = find(Node == 2, 1);

% RSSI data for each node
node1RSSI = RSSI(node1Index:node2Index - 1);
node2RSSI = RSSI(node2Index:end);

% mean of each RSSI data
node1RSSImean = mean(node1RSSI);
node2RSSImean = mean(node2RSSI);

% time array for each node
correctTime = datenum(Time, 'HH:MM:SS');
node1Time = correctTime(node1Index:node2Index-1);
node2Time = correctTime(node2Index:end);

% plot
figure(1);
hold on
plot(node1Time, node1RSSI)
plot(node2Time, node2RSSI, 'r')
plot([node1Time(1) node1Time(end)], [node1RSSImean node1RSSImean], '--k')
plot([node2Time(1) node2Time(end)], [node2RSSImean node2RSSImean], '--k')

NumTicks = 20;
L = get(gca, 'XLim');
set(gca, 'XTick', linspace(L(1), L(2), NumTicks))
datetick('x', 'HH:MM:SS', 'keeplimits', 'keepticks')
set(gca, 'XMinorTick', 'on')
minTime = min([node1Time(1) node2Time(1)]);
maxTime = max([node1Time(end) node2Time(end)]);
xlim([minTime maxTime])

title('Recieved Signal Strength')
xlabel('Time Stamp (HH:MM:SS)')
ylabel('Signal Strength (mW)')
legend('Node1', 'Node2')

%% External
% % indices of nodes
% node1Index = find(Node == 1, 1);
% node2Index = find(Node == 2, 1);

% External data for each node
node1Ext = Extrnl(node1Index:node2Index - 1);
node2Ext = Extrnl(node2Index:end);

% mean of each RSSI data
node1Extmean = mean(node1Ext);
node2Extmean = mean(node2Ext);

% time array for each node
% correctTime = datenum(Time, 'HH:MM:SS');
% node1Time = correctTime(node1Index:node2Index-1);
% node2Time = correctTime(node2Index:end);

% plot
figure(2);
hold on
plot(node1Time, node1Ext)
plot(node2Time, node2Ext, 'r')
plot([node1Time(1) node1Time(end)], [node1Extmean node1Extmean], '--k')
plot([node2Time(1) node2Time(end)], [node2Extmean node2Extmean], '--k')

NumTicks = 20;
L = get(gca, 'XLim');
set(gca, 'XTick', linspace(L(1), L(2), NumTicks))
datetick('x', 'HH:MM:SS', 'keeplimits', 'keepticks')
set(gca, 'XMinorTick', 'on')
minTime = min([node1Time(1) node2Time(1)]);
maxTime = max([node1Time(end) node2Time(end)]);
xlim([minTime maxTime])

title('External Voltage')
xlabel('Time Stamp (HH:MM:SS)')
ylabel('Voltage (mV)')
legend('Node1', 'Node2')

%% dT
% % indices of nodes
% node1Index = find(Node == 1, 1);
% node2Index = find(Node == 2, 1);

% convert dT to readable data
tempdT  = datenum(dT);
tempdT  = datevec(tempdT);
finaldT = tempdT(:,4)/60 + tempdT(:,5); 

% External data for each node
node1dT = finaldT(node1Index:node2Index - 1);
node2dT = finaldT(node2Index:end);

% mean of each RSSI data
node1dTmean = mean(node1dT);
node2dTmean = mean(node2dT);

% time array for each node
% correctTime = datenum(Time, 'HH:MM:SS');
% node1Time = correctTime(node1Index:node2Index-1);
% node2Time = correctTime(node2Index:end);

% plot
figure(3);
hold on
plot(node1Time, node1dT)
plot(node2Time, node2dT, 'r')
plot([node1Time(1) node1Time(end)], [node1dTmean node1dTmean], '--k')
plot([node2Time(1) node2Time(end)], [node2dTmean node2dTmean], '--k')

NumTicks = 20;
L = get(gca, 'XLim');
set(gca, 'XTick', linspace(L(1), L(2), NumTicks))
datetick('x', 'HH:MM:SS', 'keeplimits', 'keepticks')
set(gca, 'XMinorTick', 'on')
minTime = min([node1Time(1) node2Time(1)]);
maxTime = max([node1Time(end) node2Time(end)]);
xlim([minTime maxTime])

title('Time Between Packets Sent')
xlabel('Time Stamp (HH:MM:SS)')
ylabel('Time Between (seconds)')
legend('Node1', 'Node2')