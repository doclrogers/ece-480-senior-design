#-------------------------------------------------------------------------------
# Name:        module1
# Purpose:
#
# Author:      Brad
#
# Created:     01/12/2013
# Copyright:   (c) Brad 2013
# Licence:     <your licence>
#-------------------------------------------------------------------------------

def failData():
    samplePath = open("C:\\Users\\Brad\\repos\\senior-design\\sample\\sample1-1.txt")
    writePath  = open("C:\\Users\\Brad\\repos\\senior-design\\sample\\failed1-1.txt", 'w')
    #samplePath = open(inputFile)
    lines = samplePath.readlines()
    samplePath.close()


    failCount = 0
    startFail = int(len(lines)/2)
    endFail   = int(len(lines)*0.75)

    for line in lines:
        if line == "\n":
            line = '\n'
            writePath.write(line)

        else:

            splitLine = line.split()
            ##  ['Packet', '#', '####', '|', 'Node', '1', '|', 'TX', 'ID', '###', '|', 'Temp', '-----', 'F', '|', 'Light', '####', 'lx\n']
            ##  ['Time', '##:##:##', '|', 'dT', '##:##', '|', 'RSSI', '#.##mW', '|', 'Humidity', '##', '%', '|', 'Extrnl', '####', 'mY\n']

            if splitLine[0] == 'Packet':
                temp    = splitLine[splitLine.index('Temp') + 1]
                light   = splitLine[splitLine.index('Light') + 1]

                if startFail < failCount < endFail:
                    newlight = float(light)*0.25
                    line = line.replace(light, str(newlight))

                    newTemp = '0'
                    line = line.replace(temp, newTemp)

                writePath.write(line)

            elif splitLine[0] == "Time":
                dT   = splitLine[splitLine.index('dT') + 1]
                RSSI = splitLine[splitLine.index('RSSI') + 1][:-2] # to remove the mW
                humidity = splitLine[splitLine.index('Humidity') + 1]
                external = splitLine[splitLine.index('Extrnl') + 1]

                if startFail < failCount < endFail:
                    newRSSI = float(RSSI)*0.25
                    line = line.replace(RSSI, str(newRSSI))

                    newExt = float(external)*0.4
                    line = line.replace(external, str(newExt))


                writePath.write(line)
                #writePath.write('\n')




        failCount += 1

    writePath.close()


failData()
