#-------------------------------------------------------------------------------
# Name:        module1
# Purpose:
#
# Author:      Brad
#
# Created:     25/11/2013
# Copyright:   (c) Brad 2013
# Licence:     <your licence>
#-------------------------------------------------------------------------------

import collections
import csv
import os

def SamplesToCSV(inputFile):
    #samplePath = open("C:\\Users\\Brad\\repos\\senior-design\\sample\\sample2-3.txt")
    samplePath = open(inputFile)
    lines = samplePath.readlines()
    samplePath.close()

    nodeDict = {}
    packetList = []

    for line in lines:
        if line == "\n":
            continue

        line = line.split()
        ##  ['Packet', '#', '####', '|', 'Node', '1', '|', 'TX', 'ID', '###', '|', 'Temp', '-----', 'F', '|', 'Light', '####', 'lx\n']
        ##  ['Time', '##:##:##', '|', 'dT', '##:##', '|', 'RSSI', '#.##mW', '|', 'Humidity', '##', '%', '|', 'Extrnl', '####', 'mY\n']

        if line[0] == 'Packet':
            nodeNum = line[line.index('Node')+1]
            packetList.append([])


            if nodeNum not in nodeDict.keys():
                nodeDict[nodeNum] = []

            currPackNumber = line[2]
            txID    = line[line.index('ID') + 1]
            temp    = line[line.index('Temp') + 1]
            light   = line[line.index('Light') + 1]
            packetList[-1].extend([nodeNum, currPackNumber, txID, temp, light])

        elif line[0] == "Time":
            time = line[line.index('Time') + 1]
            dT   = line[line.index('dT') + 1]
            RSSI = line[line.index('RSSI') + 1][:-2] # to remove the mW
            humidity = line[line.index('Humidity') + 1]
            external = line[line.index('Extrnl') + 1]
            packetList[-1].extend([time, dT, RSSI, humidity, external])
            nodeDict[nodeNum].append(packetList[-1])

    baseName, extension = os.path.splitext(inputFile)
    writeFile = baseName + ".csv"


    #nodeDict = collections.OrderedDict(sorted(nodeDict.items()))

    with open(writeFile, 'wb') as fout:
        csvout = csv.writer(fout)
        csvout.writerow(['Node #', 'Packet #', 'TX ID', 'Temp', 'Light', 'Time', 'dT', 'RSSI', 'Humidity', 'Extrnl'])
        for nodeNum in nodeDict:
            for row in nodeDict[nodeNum]:
                csvout.writerow( row)

pathName = "C:\\Users\\Brad\\repos\\senior-design\\sample\\"
os.chdir(pathName)
SamplesToCSV(pathName + "sample2-3.txt")
for inFile in os.listdir("."):
    if ("sample" in inFile) and (inFile.endswith('.txt')):
        SamplesToCSV(pathName + inFile)
