# Packet Class
#
# A class to create an data packet object
#
# @author David Rogers
#
# Last Modified: 11-29-13

from datetime import datetime

class Packet:
    number = -1
    node = -1
    time = 0
    dTime = 0
    txId = 0 
    dT = 0 #in seconds
    temp = 0 #F
    light = 0 #lx
    rssi = 0 #mW
    humidity = 0 #%
    extrnl = 0 #mV
    
    def __init__(self):
        self.time = datetime.now()
        #print self.time

    def __str__(self):
        return "Node #: " + str(self.node) + "\nPacket #: " + str(self.number) + "\nTime Received: " + str(self.time) + "\n"
        
    def getData(self, metric):
        if (metric == 0): return self.temp
        elif (metric == 1): return self.light
        elif (metric == 2): return self.humidity
