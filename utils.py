# Utilities File
#
# Utility functions for processing Packets
#
# @author David Rogers
#
# Last Modified: 11-29-13

import logging
from packet import Packet

#Create logger
logger = logging.getLogger('WSN-Diagnostic.UTILS')

########################################
# Creates a Packet from a data string.
#
# Packet # 2672 | Node   2 | TX ID  ---   | Temp   72.3 F | Light   203 lx
# Time 01:09:12 | dT 00:02 | RSSI  1.85mW | Humidity 52 % | Extrnl 1689 mV
# 
# @returns A Packet.
########################################
def parsePacket(data, mode):
    logger.info('Entering parsePacket()');
    
    data = data.rstrip()
    if (mode == 1):
        data = data.split('\r\n')
        data = data[0] + ' | ' + data[1]
    elif (mode == 0):
        data = data.split('\r\n')
        data = data[0].split('\n')
        data = data[0] + ' | ' + data[1]
    
    p = Packet()
    fields = data.split(' | ')
    for field in fields:
        field = field.rstrip()
        tokens = field.split(' ')
        
        if (len(tokens) > 1):
            if (tokens[-1] == '-----' or tokens[-1] == '----' or tokens[-1] == '---' or tokens[-1] == '--'): tokens[-1] = 0
        if (len(tokens) > 2):
            if (tokens[-2] == '-----' or tokens[-2] == '----' or tokens [-2] == '---' or tokens[-2] == '--'): tokens[-2] = 0
        
        if tokens[0] == 'Packet':
            p.number = int(tokens[-1])
        elif tokens[0] == 'Node':
            p.node = int(tokens[-1])
        elif tokens[0] == 'TX':
            p.txId = int(tokens[-1])
        elif tokens[0] == 'dT':
            tmp = tokens[-1].split(':')
            p.dT = int(tmp[0])*60 + int(tmp[1])
            #print p.dT
        elif tokens[0] == 'Time':
            #Ex) Time 00:49:26
            tmp = tokens[-1].split(':')
            p.dTime = int(tmp[0])*3600 + int(tmp[1])*60 + int(tmp[2])
        elif tokens[0] == 'Temp':
            p.temp = float(tokens[-2])
        elif tokens[0] == 'Light':
            p.light = int(tokens[-2])
        elif tokens[0] == 'RSSI':
            p.rssi = float(tokens[-1][:-2])
        elif tokens[0] == 'Humidity':
            p.humidity = int(tokens[-2])
        elif tokens[0] == 'Extrnl':
            p.extrnl = int(tokens[-2])
    
    logger.info('Exiting parsePacket(): Returning Packet #' + str(p.number));
    return p

