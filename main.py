# Driver/Controller
#
# Establishes connection to cluster head on USB port 'COM3' or demo mode
# Launches data model and view
#
# @author David Rogers
#
# Last Modified: 11-29-13

#TODO Fix exiting cleanly

import os, ctypes, sys, threading, logging
import serial as COM
from time import sleep
from datetime import datetime
from packet import Packet
from utils import *
from model import DataModel
from view import View


########################################
#
# controller_d - creates Packets from sample data stream 
# and sends them to the data model for processing
#
########################################
def controller_d(dfile, model, view):
    #Start the view in a separate thread
    viewThread = threading.Thread(target=view)
    viewThread.start()
    
    data = dfile.read().split() #Pre-parse all data
    nPkt = False
    dStream = ''
    demoTime = -1
    for word in data:
        if ((word == 'Packet') and nPkt):
            pkt = parsePacket(dStream, 0)
            if (demoTime == -1): demoTime = pkt.dTime
            if (pkt.dTime-demoTime > 0): sleep(pkt.dTime-demoTime)
            demoTime = pkt.dTime
            model.addPacket(pkt)
            print dStream
            dStream = ''
        elif ((word == 'Packet') and (not nPkt)):
            nPkt = True
        elif (word == 'Time'):
            dStream += '\n'
        dStream += (word + ' ')
    
    #Process final packet
    pkt = parsePacket(dStream, 0)
    demoTime = pkt.dTime
    sleep(pkt.dTime-demoTime)
    demoTime = pkt.dTime
    model.addPacket(pkt)
    print dStream

########################################
#
# controller_r - creates Packets from real time data stream 
# and sends them to the data model for processing
#
########################################
def controller_r(connect, model, view):
    #Start the view in a separate thread
    viewThread = threading.Thread(target=view)
    viewThread.start()
    
    dStream = ''
    nPkt = False
    while True:
        data = connect.read()
        if ((data == 'P') and nPkt):
            pkt = parsePacket(dStream, 1)
            #Update Model
            model.addPacket(pkt)
            print dStream
            dStream = ''
        elif ((data == 'P') and (not nPkt)):
            nPkt = True
        dStream += data

########################################
# Configure settings and launch controller, model, and view
########################################

#Configure Terminal
ctypes.windll.kernel32.SetConsoleTitleA("PowerCast Terminal")

#Configure Logger for debug
logger = logging.getLogger('WSN-Diagnostic')
logger.setLevel(logging.DEBUG)
with open('debug.log', 'w'):
    pass
fh = logging.FileHandler('debug.log') #All logging goes to debug.log
fh.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
fh.setFormatter(formatter)
logger.addHandler(fh) # add the handler to the logger

logger.info('Logger instantiated.')

#Create Data Model
model = DataModel();

#Create View Object
gui = View(model);

logger = logging.getLogger('WSN-Diagnostic.MAIN')
mode = raw_input("(D)emo or (R)eal-time mode? ")
if (mode == 'D' or mode == 'd'):
    dfile = raw_input("Input path to data file: ")
    try:
        f = open(dfile, 'r')
        logger.info('Opened file: ' + dfile)
    except IOError:
        logger.error('Unable to open file: ' + dfile)
        print "Unable to open file: " + dfile
        print "Exiting..."
        sleep(5)
        sys.exit(1)
    try:
        controller_d(f, model, gui)
        print "Demo Complete!"
        f.close()
    except (KeyboardInterrupt, SystemExit):
        f.close()
elif (mode == 'R' or mode == 'r'):
    try:
        connect = COM.Serial('COM3', baudrate=19200)
        logger.info('Opened serial connection on port COM3.')
    except:
        logger.error('Unable to open connection on port COM3.')
        print "Unable to open connection on port COM3."
        print "Exiting..."
        sleep(2)
        sys.exit(1)
    try:
        controller_r(connect, model, gui)
    except (KeyboardInterrupt, SystemExit):
        #Be sure to close the connection
        connect.close()
        logger.info('Serial connection on port COM3 closed.')
else:
    logger.error('Invalid mode.')
    print "Invalid mode. Relaunch and select either 'D' or 'R' at the prompt."
    print "Exiting..."
    sleep(2)

print "Exiting program."
logger.info('Exiting program.')
sys.exit(0)
