Sample files for testing and demo purposes

sample1-x:
Valid data from only one node

sample2-x:
Valid data from two nodes

sample2-3:
2 Nodes
Transmitter Height: 2.5 Inches
Sensor Angle Horizontal: 0 Degrees
Sensor Angle Vertical: 3.97 Degrees
Sensor Distance: 3 Feet
Antenna Pointing at Transmitter
Temperature Sensor - Open Circuited

sample2-4:
2 Nodes
Transmitter Height: 2.5 Inches
Sensor Angle Horizontal: 0 Degrees
Sensor Angle Vertical: 3.97 Degrees
Sensor Distance: 3 Feet
Antenna Pointing at Transmitter
Light Sensor - Open Circuited

sample2-5:
2 Nodes
Transmitter Height: 2.5 Inches
Sensor Angle Horizontal: 0 Degrees
Sensor Angle Vertical: 3.97 Degrees
Sensor Distance: 3 Feet
Antenna Pointing at Transmitter
Humidity Sensor - Open Circuited

sample2-6:
2 Nodes
Transmitter Height: 2.5 Inches
Sensor Angle Horizontal: 0 Degrees
Sensor Angle Vertical: 3.97 Degrees
Sensor Distance: 3 Feet
Antenna Pointing at Transmitter
All Sensors - Open Circuited

sample2-7:
2 Nodes
Transmitter Height: 8 Feet
Sensor Angle Horizontal: 0 Degrees
Sensor Angle Vertical: 15.25 Degrees
Sensor Distance: 14.8 Feet
Antenna Pointing at Transmitter
No Sensors - Open Circuited
