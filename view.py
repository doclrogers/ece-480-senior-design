# Graphical User Interface
#
# A class for displaying incoming sensor data in real-time.
#
# @author David Rogers
#
# Last Modified: 11-30-13

#TODO - Add date option box to GUI for x scaling
#TODO - Update retrieving only the data from the model that will be displayed
#TODO - Update debug to this file, remove unused code

import os, sys, wx, logging, operator
import matplotlib
matplotlib.use('WXAgg')
from matplotlib.figure import Figure
from matplotlib.backends.backend_wxagg import \
    FigureCanvasWxAgg as FigCanvas, \
    NavigationToolbar2WxAgg as NavigationToolbar
from matplotlib.font_manager import FontProperties
from datetime import datetime
import matplotlib.dates as dt
import numpy as np
import pylab as plt

#Create logger
logger = logging.getLogger('WSN-Diagnostic')

#Declare Constants
REFRESH_RATE_SECS = 2
TEMP_TITLE = "Temperature (" + u'\N{DEGREE SIGN}' + "F)"
LIGHT_TITLE = "Light (lx)"
HUMID_TITLE = "Relative Humidity (%)"

class BoundControlBox(wx.Panel):
    """ A static box with a couple of radio buttons and a text
        box. Allows to switch between an automatic mode and a 
        manual mode with an associated value.
    """
    def __init__(self, parent, ID, label, initval, type):
        wx.Panel.__init__(self, parent, ID)
        
        self.value = initval
        
        box = wx.StaticBox(self, -1, label)
        sizer = wx.StaticBoxSizer(box, wx.VERTICAL)
        
        self.radio_auto = wx.RadioButton(self, -1, 
            label="Auto", style=wx.RB_GROUP)
        self.radio_manual = wx.RadioButton(self, -1,
            label="Manual")
        sz = 35
        if (type == 1): sz = 55
        self.manual_text = wx.TextCtrl(self, -1, 
            size=(sz,-1),
            value=str(initval),
            style=wx.TE_PROCESS_ENTER)
        
        self.Bind(wx.EVT_UPDATE_UI, self.on_update_manual_text, self.manual_text)
        self.Bind(wx.EVT_TEXT_ENTER, self.on_text_enter, self.manual_text)
        
        manual_box = wx.BoxSizer(wx.HORIZONTAL)
        manual_box.Add(self.radio_manual, flag=wx.ALIGN_CENTER_VERTICAL)
        manual_box.Add(self.manual_text, flag=wx.ALIGN_CENTER_VERTICAL)
        
        sizer.Add(self.radio_auto, 0, wx.ALL, 10)
        sizer.Add(manual_box, 0, wx.ALL, 10)
        
        self.SetSizer(sizer)
        sizer.Fit(self)
    
    def on_update_manual_text(self, event):
        self.manual_text.Enable(self.radio_manual.GetValue())
    
    def on_text_enter(self, event):
        self.value = self.manual_text.GetValue()
    
    def is_auto(self):
        return self.radio_auto.GetValue()
        
    def manual_value(self):
        return self.value


class GraphFrame(wx.Frame):
    """ The main frame of the application
    """
    title = 'Demo: WSN-Diagnostic'
    
    def __init__(self, model):
        wx.Frame.__init__(self, None, -1, self.title)
        
        self.node_plots = [0]*10
        self.model = model
        self.nodes = []
        self.data = [[] for x in xrange(10)]
        self.timeData = []
        self.metric = 0
        
        self.xmin = dt.date2num(datetime.now())
        
        self.create_menu()
        self.create_status_bar()
        self.create_main_panel()
        
        self.redraw_timer = wx.Timer(self)
        self.Bind(wx.EVT_TIMER, self.on_redraw_timer, self.redraw_timer)        
        self.redraw_timer.Start(REFRESH_RATE_SECS*1000)

    def create_menu(self):
        self.menubar = wx.MenuBar()
        
        menu_file = wx.Menu()
        m_expt = menu_file.Append(-1, "&Save plot\tCtrl-S", "Save plot to file")
        self.Bind(wx.EVT_MENU, self.on_save_plot, m_expt)
        menu_file.AppendSeparator()
        m_exit = menu_file.Append(-1, "E&xit\tCtrl-X", "Exit")
        self.Bind(wx.EVT_MENU, self.on_exit, m_exit)
                
        self.menubar.Append(menu_file, "&File")
        self.SetMenuBar(self.menubar)

    def create_main_panel(self):
        self.panel = wx.Panel(self)

        self.init_plot()
        self.canvas = FigCanvas(self.panel, -1, self.fig)

        self.xmin_control = BoundControlBox(self.panel, -1, "X min", '00:00:00', 1)
        self.xmax_control = BoundControlBox(self.panel, -1, "X max", '23:59:59', 1)
        self.ymin_control = BoundControlBox(self.panel, -1, "Y min", 0, 0)
        self.ymax_control = BoundControlBox(self.panel, -1, "Y max", 100, 0)
        
        self.temp_button = wx.Button(self.panel, -1, "Temp")
        self.Bind(wx.EVT_BUTTON, self.on_temp_button, self.temp_button)
        
        self.light_button = wx.Button(self.panel, -1, "Light")
        self.Bind(wx.EVT_BUTTON, self.on_light_button, self.light_button)
        
        self.humid_button = wx.Button(self.panel, -1, "Humidity")
        self.Bind(wx.EVT_BUTTON, self.on_humid_button, self.humid_button)
        
        #self.cb_grid = wx.CheckBox(self.panel, -1, 
        #    "Show Grid",
        #    style=wx.ALIGN_RIGHT)
        #self.Bind(wx.EVT_CHECKBOX, self.on_cb_grid, self.cb_grid)
        #self.cb_grid.SetValue(True)
        
        #self.cb_xlab = wx.CheckBox(self.panel, -1, 
        #    "Show X labels",
        #    style=wx.ALIGN_RIGHT)
        #self.Bind(wx.EVT_CHECKBOX, self.on_cb_xlab, self.cb_xlab)        
        #self.cb_xlab.SetValue(True)
        
        self.hbox1 = wx.BoxSizer(wx.HORIZONTAL)
        self.hbox1.Add(self.temp_button, border=5, flag=wx.ALL | wx.ALIGN_CENTER_VERTICAL)
        self.hbox1.AddSpacer(10)
        self.hbox1.Add(self.light_button, border=5, flag=wx.ALL | wx.ALIGN_CENTER_VERTICAL)
        self.hbox1.AddSpacer(10)
        self.hbox1.Add(self.humid_button, border=5, flag=wx.ALL | wx.ALIGN_CENTER_VERTICAL)
        #self.hbox1.AddSpacer(20)
        #self.hbox1.Add(self.cb_grid, border=5, flag=wx.ALL | wx.ALIGN_CENTER_VERTICAL)
        #self.hbox1.AddSpacer(10)
        #self.hbox1.Add(self.cb_xlab, border=5, flag=wx.ALL | wx.ALIGN_CENTER_VERTICAL)
        
        self.hbox2 = wx.BoxSizer(wx.HORIZONTAL)
        self.hbox2.Add(self.xmin_control, border=5, flag=wx.ALL)
        self.hbox2.Add(self.xmax_control, border=5, flag=wx.ALL)
        self.hbox2.AddSpacer(24)
        self.hbox2.Add(self.ymin_control, border=5, flag=wx.ALL)
        self.hbox2.Add(self.ymax_control, border=5, flag=wx.ALL)
        
        self.vbox = wx.BoxSizer(wx.VERTICAL)
        self.vbox.Add(self.canvas, 1, flag=wx.LEFT | wx.TOP | wx.GROW)        
        self.vbox.Add(self.hbox1, 0, flag=wx.ALIGN_CENTER | wx.TOP)
        self.vbox.Add(self.hbox2, 0, flag=wx.ALIGN_CENTER | wx.TOP)
        
        self.panel.SetSizer(self.vbox)
        self.vbox.Fit(self)
    
    def create_status_bar(self):
        self.statusbar = self.CreateStatusBar()

    def init_plot(self):
        self.dpi = 100
        self.fig = Figure((3.0, 3.0), dpi=self.dpi)

        self.axes = self.fig.add_subplot(111)
        self.axes.set_axis_bgcolor('black')
        self.axes.set_title(TEMP_TITLE, size=12)
        
        
        plt.setp(self.axes.get_xticklabels(), fontsize=8)
        plt.setp(self.axes.get_yticklabels(), fontsize=8)
        
        #Shrink plot in order to fit legend
        box = self.axes.get_position()
        self.axes.set_position([box.x0, box.y0, box.width * 0.85, box.height])

        self.fontP = FontProperties()
        self.fontP.set_size('small')
   
    def init_node(self, number):
    
        #avoid red and green for color blind
        if (len(self.nodes) == 1):
            clr = '#FFFF00' #yellow
        elif (len(self.nodes) == 2):
            clr = '#00FFFF' #cyan
        elif (len(self.nodes) == 3):
            clr = '#FF00FF' #magenta   
        elif (len(self.nodes) == 4):
            clr = '#660066' #purple  
        elif (len(self.nodes) == 5):
            clr = '#0000FF' #blue  
        elif (len(self.nodes) == 6):
            clr = '#FF9900' #orange 
        elif (len(self.nodes) == 7):
            clr = '#FF9999' #pink 
        elif (len(self.nodes) == 8):
            clr = '#A0A0A0' #grey 
        elif (len(self.nodes) == 9):
            clr = '#99FF00' #lime green
        elif (len(self.nodes) == 10):
            clr = '#CCCCCC' #blue grey 
        
        node_line = self.axes.plot_date(
        [0], [1],
        xdate = True,
        ydate = False,
        linewidth=1,
        linestyle='-',
        color=clr,
        label="Node " + str(number)
        )[0]

        self.node_plots[number] = node_line
            
    def draw_plot(self):
        """ Redraws the plot
        """
        # Sliding window of ~90 seconds of data (0.0037)
        xdiff = self.timeData[-1] - self.timeData[0]
        if self.xmax_control.is_auto():
            if (xdiff < 0.001):
                xmax = self.timeData[0]+0.001
            else:
                xmax = self.timeData[-1]+0.0001
        else:
            #TODO - error checking
            input = self.xmax_control.manual_value().split(':')
            tdy = datetime.now().replace(hour=int(input[0]),minute=int(input[1]),second=int(input[2]))
            xmax = dt.date2num(tdy)
                
        if self.xmax_control.is_auto() and self.xmin_control.is_auto():            
            if (xdiff < 0.001):
                xmin = self.timeData[0]
            else:
                xmin = self.timeData[-1]-0.001
        elif self.xmin_control.is_auto():
            xmin = self.xmin
        else:
            #TODO - error checking
            input = self.xmin_control.manual_value().split(':')
            tdy = datetime.now().replace(hour=int(input[0]),minute=int(input[1]),second=int(input[2]))
            xmin = dt.date2num(tdy)
        
        # for ymin and ymax, find the minimal and maximal values
        # in the data set and add a minimal margin.
        # 
        # note that it's easy to change this scheme to the 
        # minimal/maximal value in the current display, and not
        # the whole data set.
        # 
        if (self.ymin_control.is_auto() or self.ymax_control.is_auto()):
            maxy = 0
            miny = 99999
            for node in self.nodes:
                maxi = max(self.data[node][-5:])
                mini = min(self.data[node][-5:])
                if (maxi > maxy):
                    maxy = maxi
                if (mini < miny):
                    miny = mini
        
        if self.ymin_control.is_auto():
            ymin = miny*0.95 - 1
        else:
            ymin = int(self.ymin_control.manual_value())
        
        if self.ymax_control.is_auto():
            ymax = maxy*1.05 + 1
        else:
            ymax = int(self.ymax_control.manual_value())
        
        self.xmin = xmin
        self.axes.set_xbound(lower=xmin, upper=xmax)
        self.axes.set_ybound(lower=ymin, upper=ymax)
        self.axes.grid(True, color='gray')

        #Display marks every 30 seconds
        if (self.xmin_control.is_auto() and self.xmax_control.is_auto()):
            self.axes.xaxis.set_major_locator(dt.SecondLocator(interval=30))
            self.axes.xaxis.set_major_formatter(dt.DateFormatter('%H:%M:%S'))
        else:
            self.axes.xaxis.set_major_locator(dt.AutoDateLocator())
            if (xmax-xmin < 0.005):
                self.axes.xaxis.set_major_formatter(dt.DateFormatter('%H:%M:%S'))
            else:
                self.axes.xaxis.set_major_formatter(dt.DateFormatter('%H:%M'))
        
        for node in self.nodes:
            self.node_plots[node].set_xdata(self.timeData)
            self.node_plots[node].set_ydata(np.array(self.data[node]))
        
        #Add sorted legend
        handles, labels = self.axes.get_legend_handles_labels()
        hl = sorted(zip(handles, labels),
            key=operator.itemgetter(1))
        handles2, labels2 = zip(*hl)
        
        #Put a legend below current axis
        self.axes.legend(handles2, labels2, loc='center left', bbox_to_anchor=(1, 0.5),
          fancybox=True, ncol=1, prop=self.fontP)
        
        self.canvas.draw()
    
    def on_temp_button(self, event):
        if (self.metric != 0):
            self.model.changeMetric(0);
            self.metric = 0
            for node in self.nodes:
                self.data[node] = self.model.getSensorData(node)
            self.axes.set_title(TEMP_TITLE, size=12)
    
    def on_light_button(self, event):
        if (self.metric != 1):
            self.model.changeMetric(1);
            self.metric = 1
            for node in self.nodes:
                self.data[node] = self.model.getSensorData(node)
            self.axes.set_title(LIGHT_TITLE, size=12)
    
    def on_humid_button(self, event):
        if (self.metric != 2):
            self.model.changeMetric(2);
            self.metric = 2
            for node in self.nodes:
                self.data[node] = self.model.getSensorData(node)
            self.axes.set_title(HUMID_TITLE, size=12)
    
    #def on_cb_grid(self, event):
    #    self.draw_plot()
    
    #def on_cb_xlab(self, event):
    #    self.draw_plot()
    
    def on_save_plot(self, event):
        file_choices = "PNG (*.png)|*.png"
        
        dlg = wx.FileDialog(
            self, 
            message="Save plot as...",
            defaultDir=os.getcwd(),
            defaultFile="plot.png",
            wildcard=file_choices,
            style=wx.SAVE)
        
        if dlg.ShowModal() == wx.ID_OK:
            path = dlg.GetPath()
            self.canvas.print_figure(path, dpi=self.dpi)
            self.flash_status_message("Saved to %s" % path)
    
    def on_redraw_timer(self, event):
        if (self.model.isNewNode()):
            nodeId = self.model.getNewNode()
            self.nodes.append(nodeId)
            self.init_node(nodeId)
        
        if (len(self.nodes) == 0):
            return
        
        for node in self.nodes:
            self.data[node] = self.model.getSensorData(node)
        
        self.timeData = dt.date2num(self.model.getTimes())
        self.draw_plot()
    
    def on_exit(self, event):
        self.Destroy()
    
    def flash_status_message(self, msg, flash_len_ms=1500):
        self.statusbar.SetStatusText(msg)
        self.timeroff = wx.Timer(self)
        self.Bind(
            wx.EVT_TIMER, 
            self.on_flash_status_off, 
            self.timeroff)
        self.timeroff.Start(flash_len_ms, oneShot=True)
    
    def on_flash_status_off(self, event):
        self.statusbar.SetStatusText('')
        
        
class View:
    def __init__(self, model):
        self.logger = logging.getLogger('WSN-Diagnostic.VIEW')
        self.logger.info('Creating an instance of the view.')
        self.model = model
        
    def __call__(self):
        self.logger.info('Creating view application for display.')
        self.app = wx.PySimpleApp()
        self.app.frame = GraphFrame(self.model)
        self.app.frame.Show()
        self.app.MainLoop()