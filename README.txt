﻿Michigan State University
ECE 480 - Senior Design Team 2

David Rogers
Brad Garrod
Stu Andrzejewski
Kelly Desmond

----------------------------------------------------------------
-
- Software Installation
-
----------------------------------------------------------------
This program runs using python 2.7.x and the following modules:

pySerial
http://pyserial.sourceforge.net/pyserial.html

wxPython
http://wxpython.org/

matplotlib w/ numpy
http://matplotlib.org/

test
----------------------------------------------------------------
-
- Running the Application 
-
----------------------------------------------------------------

Simply double-click main.py and the application should launch properly.
If not, be sure that all the proper modules are installed.

In real-time mode the development board must be connected on port 'COM3'.

----------------------------------------------------------------
-
- Misc Information
-
----------------------------------------------------------------

Switch S1
Resets the system software and all data buffers, including time counter and dT counter. Pressing S1 is
not required to start receiving packets and displaying sensor data.

Switch S2
Outputs data to the screen from the last packet received with an updated elapsed time and time
differential (dT).

915 MHz PCB Dipole Antenna
This antenna is flat and has the RF connector located at the bottom of the antenna.
Type: omni-directional, vertically polarized
Energy pattern: 360°
Antenna gain: Linear gain = 1.25 (1.0 dBi)

915 MHz PCB Patch Antenna
This antenna has two layers and the RF connector located on the back of the antenna. The front side should be
pointed toward the transmitter with the same polarization
Type: directional, vertically polarized
Energy pattern: 122° (azimuth/horizontal), 68° (elevation/vertical)
Antenna gain: Linear gain = 4.1 (6.1 dBi)

JP2 can be used to measure current being used by the sensor board. It should be connected for
normal operation.

The sensor modules will transmit data when Vout of the P2110 is activated.

Packet # 2672 | Node   2 | TX ID  ---   | Temp   72.3 F | Light   203 lx
Time 01:09:12 | dT 00:02 | RSSI  1.85mW | Humidity 52 % | Extrnl 1689 mV

Packet # 2673 | Node   2 | TX ID  242   | Temp   72.3 F | Light   206 lx
Time 01:09:15 | dT 00:03 | RSSI  2.22mW | Humidity 52 % | Extrnl 1679 mV

Packet # 2674 | Node   1 | TX ID  ---   | Temp   72.1 F | Light   229 lx
Time 01:09:17 | dT 00:06 | RSSI  1.08mW | Humidity 52 % | Extrnl 1111 mV

Packet # 2675 | Node   2 | TX ID  242   | Temp   72.3 F | Light   202 lx
Time 01:09:17 | dT 00:02 | RSSI  1.97mW | Humidity 52 % | Extrnl 1679 mV

Packet # 2676 | Node   2 | TX ID  242   | Temp   72.3 F | Light   202 lx
Time 01:09:19 | dT 00:02 | RSSI  2.08mW | Humidity 52 % | Extrnl 1679 mV

Packet # 2677 | Node   2 | TX ID  242   | Temp   72.1 F | Light   200 lx
Time 01:09:21 | dT 00:02 | RSSI  2.08mW | Humidity 55 % | Extrnl 1692 mV


10 feet - every second
20 feet - every 6 seconds
30 feet - every 21 seconds
40 feet - every 89 seconds
