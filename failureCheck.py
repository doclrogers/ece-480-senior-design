#-------------------------------------------------------------------------------
# Name:        module1
# Purpose:
#
# Author:      Brad
#
# Created:     01/12/2013
# Copyright:   (c) Brad 2013
# Licence:     <your licence>
#-------------------------------------------------------------------------------

def getData():
    samplePath = open("C:\\Users\\Brad\\repos\\senior-design\\sample\\sample2-3.txt")
    #samplePath = open(inputFile)
    lines = samplePath.readlines()
    samplePath.close()

    nodeDict = {}
    packetList = []

    for line in lines:
        if line == "\n":
            continue

        line = line.split()
        ##  ['Packet', '#', '####', '|', 'Node', '1', '|', 'TX', 'ID', '###', '|', 'Temp', '-----', 'F', '|', 'Light', '####', 'lx\n']
        ##  ['Time', '##:##:##', '|', 'dT', '##:##', '|', 'RSSI', '#.##mW', '|', 'Humidity', '##', '%', '|', 'Extrnl', '####', 'mY\n']

        if line[0] == 'Packet':
            nodeNum = line[line.index('Node')+1]
            packetList.append([])


            if nodeNum not in nodeDict.keys():
                nodeDict[nodeNum] = []

            currPackNumber = line[2]
            txID    = line[line.index('ID') + 1]
            temp    = line[line.index('Temp') + 1]
            light   = line[line.index('Light') + 1]
            packetList[-1].extend([nodeNum, currPackNumber, txID, temp, light])

        elif line[0] == "Time":
            time = line[line.index('Time') + 1]
            dT   = line[line.index('dT') + 1]
            RSSI = line[line.index('RSSI') + 1][:-2] # to remove the mW
            humidity = line[line.index('Humidity') + 1]
            external = line[line.index('Extrnl') + 1]
            packetList[-1].extend([time, dT, RSSI, humidity, external])
            nodeDict[nodeNum].append(packetList[-1])

    return nodeDict

import numpy

def shortTermAnalysis(dataList):
    ''' to be used with external sensors '''

    dataLength = 4      # use 4 samples to compare against
    threshold  = 10     # the change that is considered something wrong with sensor

    if len(dataList) < dataLength:
        compareData = dataList
    else:
        compareData = dataList[(len(dataList) - dataLength):]

    compareAvg = numpy.mean(compareData[0:-1]) # get mean of the data except the latest

    # use a threshold to determine if the ndoe should be looked at
    if abs(compareAvg - compareData[-1]) > threshold:
        # compares last data value versus mean to see if sudden change
        return True
    else:
        return False

def longTermAnalysis(dataList):
    ''' to be used with internal sensors
        The internal sensors should be pretty consistent, so if it waivers beyond
            the threshold calculated at 20% of the mean of the signal then
            we can assume that something is not operating correctly'''

    compareAvg = numpy.mean(dataList)
    threshold = compareAvg - compareAvg*0.2

    if abs(compareAvg - dataList[-1]) > threshold:
        print "LONG TERM FAILURE -- "
        print "DATA       ",dataList[-1]
        print "MEAN       ", compareAvg
        return True
    else:
        return False

def zeroCheck(dataList):
    ''' to be used with all sensors
        Function checks if there was data that then started transmitting 0's,
        indicating something is not working correctly '''

    dataLength = 4      # use 4 samples to compare against
    threshold  = 5     # the change that is considered something wrong with sensor

    if len(dataList) < dataLength:
        compareData = dataList
    else:
        compareData = dataList[(len(dataList) - dataLength):]

    compareAvg = numpy.mean(compareData[0:-1]) # get mean of the data except the latest

    if compareAvg < threshold: # says that zeros cannot give accurate data
        return False


    #
    try:
        if (dataList[-3] != 0) and (dataList[-2] != 0) and (dataList[-1] == 0):
            return True

    except: # not enough data
        return False


nodeDict = getData()
nodeOne = nodeDict['1']

nodeOneRSSI = []
for dataList in nodeOne:
    nodeOneRSSI.append(float(dataList[-3]))

    longFailure = longTermAnalysis(nodeOneRSSI)
    if longFailure == True:
        print longFailure

    shortFailure = shortTermAnalysis(nodeOneRSSI)
    if shortFailure == True:
        print shortFailure

    zeroFailure = zeroCheck(nodeOneRSSI)
    if zeroFailure == True:
        print zeroFailure








