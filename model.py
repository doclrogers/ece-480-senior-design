# Data Model
#
# A class for organizing all incoming and previous data
#
# @author David Rogers
#
# Last Modified: 11-30-13

import sys, logging, Queue

#TODO - Declare timed out nodes and reset all values to 0 from last heard time on time out
#TODO - Create API call for retrieving data for specific time range
#TODO - Integrate detection algorithms

#Create logger
logger = logging.getLogger('WSN-Diagnostic')

class DataModel:
    currentMetric = 0
    
    #Node list - Auto detect which node numbers exist in the network
    validNodes = []
    newNodes = Queue.Queue(10);
    
    #Lists for holding data for each node
    #Ex) tempData[1][2] holds the temperature value for
    #node 1 at time timeData[2]
    timeData = []
    nodeTimeData = [[] for x in xrange(10)]
    lastHeardData = [[] for x in xrange(10)]
    tempData = [[] for x in xrange(10)]   #Metric 0
    lightData = [[] for x in xrange(10)]  #Metric 1
    humidData = [[] for x in xrange(10)]  #Metric 2
    
    def __init__(self):
        self.logger = logging.getLogger('WSN-Diagnostic.MODEL')
        self.logger.info('Creating an instance of the data model.')

    def addPacket(self, pkt):
        if (pkt.node not in self.validNodes) and (len(self.timeData) > 0):
            self.validNodes.append(pkt.node) #add node number to list
            self.newNodes.put(pkt.node)
            #Zero pad the beginning timestamps
            self.tempData[pkt.node] = [0]*len(self.timeData)
            self.lightData[pkt.node] = [0]*len(self.timeData)
            self.humidData[pkt.node] = [0]*len(self.timeData)
        
        self.timeData.append(pkt.time)
        self.lastHeardData[pkt.node] = pkt.time
        self.nodeTimeData[pkt.node].append(pkt.time)
        self.tempData[pkt.node].append(pkt.temp)
        self.lightData[pkt.node].append(pkt.light)
        self.humidData[pkt.node].append(pkt.humidity)
        
        #Maintain even length data
        for i in self.validNodes:
            if (i != pkt.node):
                self.tempData[i].append(self.tempData[i][-1])
                self.lightData[i].append(self.lightData[i][-1])
                self.humidData[i].append(self.humidData[i][-1])
        
        
    def changeMetric(self, metric):
        self.currentMetric = metric
   
    def getSensorData(self, node):
        if (self.currentMetric == 0):
            return self.tempData[node]
        elif (self.currentMetric == 1):
            return self.lightData[node]
        elif (self.currentMetric == 2):
            return self.humidData[node]
    
    def getNodeTimeData(self, node):
        return self.nodeTimeData[node]
    
    def getTimes(self):
        return self.timeData
    
    def isNewNode(self):
        return not self.newNodes.empty()
    
    def getNewNode(self):
        return self.newNodes.get()